# Purpose
This role is a companion role to the dledanseur.letsencrypt role.

It creates the letsencrypt user and role, so that the same set of certificate created on a webserver
can be shared on other servers (smtp or imap server for instance).

To that extend, the created uid and gid are 1999, as in the letsencrypt role

Additionally, this role supports the addition of some other users in the letsencrypt group

# Variables
letsencrypt_group: 			# Name of the letsencrypt group to create
letsencrypt_granted_users: [] 		# List of users to add to the letsencrypt group


